# JK3 Linux Setup Ubuntu

How to setup a Jedi Knight: Jedi Academy Server with JA+ on Ubuntu 20.04 LTS

_**Note:** This may be incomplete, it was written from memory. It also doesn't include some basic server hardening that you should do on every server._

## Requirements & Files

* 32-bit Libraries (if you're running 64-bit): `apt install lib32z1`
* JK3/JA Linux Dedicated Server Binaries: https://jkhub.org/files/file/390-jedi-academy-dedicated-server-for-linux/
* JA+ Server Side Mod: https://jkhub.org/files/file/953-ja-server-side/
* JK3/JA Assets: assets0.pk3, assets1.pk3 and assets2.pk3, etc from your games "base" folder

## Initial Configuration

1. If you haven't already, create yourself a user and create a service user for the JK3 server to run as. Once created, test your login in a *new window*, once you're logged in. Exit out of your `root` shell in the other window.

**Create user**
```sh
adduser naomia
```

**Set your password (or install your public key at /home/naomia/.ssh/authorized_keys)**
```
passwd naomia
```

**Create service user**
```sh
adduser jads
```

**Create an administrators group**
```sh
groupadd -g 1200 admin
```

**Add your new user to the administrators group and the service user group**
```sh
usermod -a -G admin naomia
usermod -a -G jads naomia
```

**Create a sudoers file for the members of the new administrators group**
**/etc/sudoers.d/admin**
```sh
%admin   ALL=(ALL:ALL) ALL
```

2. Create your JK3/JA dedicated server working directory and create a symlink in your home directory for easier SFTP management.

**Create directory, set permissions and set group permission inheritance**
```sh
sudo mkdir -p /jads/base
sudo chown -R jads:jads /jads
sudo chmod 0770 /jads
sudo chmod g+s /jads
```

**Create a symbolic link in your home directory to the dedicated server directory**
```sh
ln -s /jads /home/naomia/jads
```

## Configure Dedicated Server

1. Connect to your SFTP server (sftp command or WinSCP). Start copying over your JA+ and JK:JA Dedicated Server files. Copy your *.pk3 files from your retail game install as well as any others you might have into `/jads/base`. 

2. Satisfy server requirements (using shell)

**If you're running a 64-bit OS, you'll need to load the 32-bit Binaries**
```sh
sudo apt install lib32z1
```

**Copy libcxa.so.1 into one of your servers checked library paths**
```sh
cp /jads/libcxa.so.1 /usr/lib/
```

3. Modify your server.cfg and other configuration files (if desired). If you're using a mod such as JA+ you'll want to use the *.cfg files in `/jads/<MOD>` e.g. `/jads/japlus`. Plenty to configure here, may update this with more detail later. 

4. Make your dedicated server binary executable

```sh
chmod u+x /jads/linuxjampded
```

**Optionally if you have a startup script, make that executable as well**
```sh
chmod u+x /jads/start_japlus_linux_autoRestart.sh
```

5. Start your server directly or using the start script. Recommend using screen or a similar utility. 

**Directly**
```sh
sudo su jads
cd /jads
./linuxjampded +set fs_game japlus +set dedicated 2 +set net_ip 1.3.3.7 +set net_port 29070 +exec server.cfg
```

**JA+ Startup Script**
```sh
sudo su jads
cd /jads
./start_japlus_linux_autoRestart.sh
```

